import commonConfig from './config';

let config = {
  ENV: 'DEV',
  CLIENTS: {
    FITBIT: {
      protocol: 'https',
      host: 'www.fitbit.com',
      pathname: '/oauth2/authorize',
      query: {
        response_type: 'token',
        client_id: '227NS7',
        redirect_uri: 'http://localhost:3000/auth/',
        scope: [
          'activity',
          'nutrition',
          'heartrate',
          'location',
          'nutrition',
          'profile',
          'settings',
          'sleep',
          'social',
          'weight'
        ]
      }
    }
  }
};

export default Object.assign({}, commonConfig, config);

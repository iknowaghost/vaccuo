import MODALS_CONSTANTS from 'store/constants/modal';
import { createReducer } from 'store/utils/redux';

/**
 * The initial state.
 * @type {Object}
 */
const initialState = {
  name: null,
  data: null
};

/**
 * Open the modal.
 *
 * @method
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const open = (state:Object, data:Object):Object => {
  
  return {
    name: data.name,
    data: data.data
  };
}

/**
 * Close the modal.
 *
 * @method
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const close = ():Object => {
  return initialState;
}

export default createReducer(initialState, {
  [MODALS_CONSTANTS.MODAL_SHOW]: open,
  [MODALS_CONSTANTS.MODAL_HIDE]: close
});

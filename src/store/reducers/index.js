import { combineReducers } from 'redux';

// reducers
import ModalReducer from './modal';
import SideBarReducer from './sideBar';
import CatalogueReducer from './catalogue';

/**
 * Combine all reducers.
 */
const appReducer = combineReducers({
  modal: ModalReducer,
  sideBar: SideBarReducer,
  catalogue: CatalogueReducer
});

/**
 * Delegates to the appReducer.
 *
 * @method
 * @private
 *
 * @param  {Object} state  The state object
 * @param  {Object} action The action
 * @return {Object}
 */
const rootReducer = (state:?Object, action:Object):Object => appReducer(state, action);

export default rootReducer;

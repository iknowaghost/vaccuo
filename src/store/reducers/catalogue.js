import CATALOGUE_CONSTANTS from 'store/constants/catalogue';
import { createReducer } from 'store/utils/redux';

/**
 * The initial state.
 * @type {Object}
 */
const initialState = {
  catalogue: [],
  isLoading: false
};

/**
 * Fetch catalogue success
 *
 * @method
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const fetchCatalogueSuccess = (state:Object, data:Object):Object => {

  return {
    ...state,
    catalogue: data,
    isLoading: false
  };
}

/**
 * Fetch catalogue request
 *
 * @method
 *
 * @param  {Object} state The state
 *
 * @return {Object} The current state
 */
const fetchCatalogueRequest = (state:Object):Object => {

  return {
    ...state,
    catalogue: [],
    isLoading: true
  };
}

/**
 * Search catalogue success
 *
 * @method
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const searchCatalogueSuccess = (state:Object, data:Object):Object => {

  return {
    ...state,
    catalogue: data,
    isLoading: false
  };
}

/**
 * Search catalogue request
 *
 * @method
 *
 * @param  {Object} state The state
 *
 * @return {Object} The current state
 */
const searchCatalogueRequest = (state:Object):Object => {

  return {
    ...state,
    catalogue: [],
    isLoading: true
  };
}

export default createReducer(initialState, {
  [CATALOGUE_CONSTANTS.FETCH_CATALOGUE_REQUEST]: fetchCatalogueRequest,
  [CATALOGUE_CONSTANTS.FETCH_CATALOGUE_SUCCESS]: fetchCatalogueSuccess,
  [CATALOGUE_CONSTANTS.SEARCH_CATALOGUE_REQUEST]: searchCatalogueRequest,
  [CATALOGUE_CONSTANTS.SEARCH_CATALOGUE_SUCCESS]: searchCatalogueSuccess
});

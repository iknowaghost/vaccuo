import { createReducer } from 'store/utils/redux';
import SIDE_BAR_CONSTANTS from 'store/constants/sideBar';
import MENU from 'store/constants/menu';

/**
 * The initial state.
 * @type {Object}
 */
const initialState = {
  isOpen: false,
  tree: MENU,
  selected: {}
};

/**
 * Open the modal.
 *
 * @method
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const open = (state:Object):Object => {

  return {
    ...state,
    isOpen: true
  };
}

/**
 * Close the modal.
 *
 * @method
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const close = ():Object => {
  return initialState;
}

/**
 * Select category.
 *
 * @method
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const updateMenu = (state:Object, data:Object):Object => {
  return {
    ...state,
    tree: [].concat(data.tree)
  }
}

/**
 * Select category.
 *
 * @method
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const selectMenuData = (state:Object, data:Object):Object => {
  return {
    ...state,
    selected: data.data
  }
}

export default createReducer(initialState, {
  [SIDE_BAR_CONSTANTS.SIDE_BAR_OPEN]: open,
  [SIDE_BAR_CONSTANTS.SIDE_BAR_CLOSE]: close,
  [SIDE_BAR_CONSTANTS.SIDE_BAR_UPDATE_MENU]: updateMenu,
  [SIDE_BAR_CONSTANTS.SIDE_BAR_SELECT_MENU_DATA]: selectMenuData
});

import CATALOGUE_CONSTANTS from 'store/constants/catalogue';
import { ActionType } from 'store/types/ActionType';

/**
 * Dispatch an action to request the catalogue.
 *
 * @method fetchCatalogueRequest
 * @public
 *
 * @return {Object}
 */
export const fetchCatalogueRequest = ():ActionType => (
  {
    type: CATALOGUE_CONSTANTS.FETCH_CATALOGUE_REQUEST
  }
)

/**
 * Dispatch an action to the success of the fetch.
 *
 * @method open
 * @public
 *
 * @param {Array} catalogue The catalogue list
 *
 * @return {Object}
 */
export const fetchCatalogueSuccess = (catalogue:Array):ActionType => (
  {
    type: CATALOGUE_CONSTANTS.FETCH_CATALOGUE_SUCCESS,
    payload: catalogue
  }
)

/**
 * Dispatch an action to request the catalogue.
 *
 * @method fetchCatalogueRequest
 * @public
 *
 * @param {String} query The query to search for
 * @return {Object}
 */
export const searchCatalogueRequest = (query:?string):ActionType => (
  {
    type: CATALOGUE_CONSTANTS.SEARCH_CATALOGUE_REQUEST,
    payload: {
      query
    }
  }
)

/**
 * Dispatch an action to the success of the search.
 *
 * @method open
 * @public
 *
 * @param {Array} products The products list
 *
 * @return {Object}
 */
export const searchCatalogueSuccess = (products:Array):ActionType => (
  {
    type: CATALOGUE_CONSTANTS.SEARCH_CATALOGUE_SUCCESS,
    payload: products
  }
)

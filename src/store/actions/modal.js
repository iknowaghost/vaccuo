import MODAL_CONSTANTS from './../constants/modal';
import { ActionType } from 'store/types/ActionType';

/**
 * Dispatch an action to show a modal.
 *
 * @method open
 * @public
 *
 * @param {String} name The name of the modal to be open
 * @param {String} data The data to be sent to the modal
 *
 * @return {Object}
 */
export const show = (name: string, data:?Object):ActionType => (
  {
    type: MODAL_CONSTANTS.MODAL_SHOW,
    payload: {
      name,
      data
    }
  }
);


/**
 * Dispatch an action to hide a modal
 *
 * @method hide
 * @public
 *
 * @return {Object}
 */
export const hide = ():ActionType => (
  {
    type: MODAL_CONSTANTS.MODAL_HIDE
  }
);

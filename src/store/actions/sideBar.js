import SIDE_BAR_CONSTANTS from './../constants/sideBar';
import { ActionType } from 'store/types/ActionType';

/**
 * Dispatch an action to open the side bar.
 *
 * @method open
 * @public
 *
 * @return {Object}
 */
export const open = ():ActionType => (
  {
    type: SIDE_BAR_CONSTANTS.SIDE_BAR_OPEN
  }
);


/**
 * Dispatch an action to close the side bar
 *
 * @method close
 * @public
 *
 * @return {Object}
 */
export const close = ():ActionType => (
  {
    type: SIDE_BAR_CONSTANTS.SIDE_BAR_CLOSE
  }
);

/**
 * Dispatch an action to update the menu
 *
 * @method updateMenu
 * @public
 *
 * @param {Array<Object>} tree The tree Array
 * @return {Object}
 */
export const updateMenu = (tree:Array<Object>):ActionType => {

  return {
    type: SIDE_BAR_CONSTANTS.SIDE_BAR_UPDATE_MENU,
    payload: {
      tree
    }
  }
}

/**
 * Dispatch an action to select a category
 *
 * @method selectMenuData
 * @public
 *
 * @param {Object} data The selected menu data
 * @return {Object}
 */
export const selectMenuData = (data:Object):ActionType => {

  return {
    type: SIDE_BAR_CONSTANTS.SIDE_BAR_SELECT_MENU_DATA,
    payload: {
      data
    }
  }
}

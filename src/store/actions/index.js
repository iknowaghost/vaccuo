import * as modalActionCreators from './modal';
import * as sideBarActionCreators from './sideBar';
import * as catalogueActionCreators from './catalogue';

export {
  modalActionCreators,
  sideBarActionCreators,
  catalogueActionCreators
};

import { createConstants } from 'store/utils/redux';

const sideBarConstants = createConstants(
  'SIDE_BAR_OPEN',
  'SIDE_BAR_CLOSE',
  'SIDE_BAR_UPDATE_MENU',
  'SIDE_BAR_SELECT_MENU_DATA'
);

export default sideBarConstants;

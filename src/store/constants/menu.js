import Item from 'components/ui/collapse-menu/Item';

export default [
  {
    id: '1',
    name: 'About',
    route: '/about',
    icon: 'about-icon.svg',
    isOpen: false,
    customComponent: Item,
    customClass: 'item'
  },
  {
    id: '2',
    name: 'Catalogue',
    route: 'catalogue',
    icon: 'catalogue-icon.svg',
    isOpen: false,
    customComponent: Item,
    customClass: 'item',
    children: [
      {
        id: '2.1',
        name: 'Chairs',
        route: '/catalogue/chairs',
        category: 'chairs',
        isOpen: false,
        customComponent: Item,
        customClass: 'subItem'
      },
      {
        id: '2.2',
        name: 'Sofas',
        route: '/catalogue/sofas',
        category: 'sofas',
        isOpen: false,
        customComponent: Item,
        customClass: 'subItem'
      },
      {
        id: '2.3',
        name: 'Tables',
        route: '/catalogue/tables',
        category: 'tables',
        isOpen: false,
        customComponent: Item,
        customClass: 'subItem'
      },
      {
        id: '2.4',
        name: 'Bookcases',
        route: '/catalogue/bookcases',
        category: 'bookcases',
        isOpen: false,
        customComponent: Item,
        customClass: 'subItem'
      },
      {
        id: '2.5',
        name: 'Pouffes',
        route: '/catalogue/pouffes',
        category: 'pouffes',
        isOpen: false,
        customComponent: Item,
        customClass: 'subItem'
      },
      {
        id: '2.6',
        name: 'Coat Stand',
        route: '/catalogue/coat-stand',
        category: 'coat-stand',
        isOpen: false,
        customComponent: Item,
        customClass: 'subItem'
      }
    ]
  },
  {
    id: 3,
    name: 'Contact',
    route: '/contact',
    isOpen: false,
    icon: 'contact-icon.svg',
    customComponent: Item,
    customClass: 'item'
  }
]

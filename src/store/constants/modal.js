import { createConstants } from 'store/utils/redux';

const modalsConstants = createConstants(
  'MODAL_SHOW',
  'MODAL_HIDE'
);

export default modalsConstants;

export const MODAL_NAMES = {
  SEARCH_MODAL: 'SEARCH_MODAL'
}

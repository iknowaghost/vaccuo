import { fork } from 'redux-saga/effects';
import { catalogue } from './catalogue/';

/**
 * Saga root.
 */
export default function* root():void {
  yield fork(catalogue);
}

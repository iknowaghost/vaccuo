import { put, call } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga';
import { catalogueService } from 'core/services'
import CATALOGUE_CONSTANTS from 'store/constants/catalogue';

/**
 * Search the catalogue.
 *
 * @param {Object} action The action creator
 */
function *searchCatalogue(action:Object):void {
  try {
    const catalogue = yield call(catalogueService.search, action.payload.query);
    yield put({ type: CATALOGUE_CONSTANTS.SEARCH_CATALOGUE_SUCCESS, payload: catalogue });
  } catch (error) {
    yield put({ type: CATALOGUE_CONSTANTS.ERROR });
  }
}

/**
 * Watch search catalogue saga.
 */
export function* watchSearchCatalogue():void {
  yield* takeLatest(CATALOGUE_CONSTANTS.SEARCH_CATALOGUE_REQUEST, searchCatalogue);
}

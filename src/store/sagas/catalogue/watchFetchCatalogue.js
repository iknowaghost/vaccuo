import { put, call } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import { catalogueService } from 'core/services'
import CATALOGUE_CONSTANTS from 'store/constants/catalogue';

/**
 * Fetch the catalogue.
 */
function *fetchCatalogue():void {
  try {
    const catalogue = yield call(catalogueService.getAll);
    yield put({ type: CATALOGUE_CONSTANTS.FETCH_CATALOGUE_SUCCESS, payload: catalogue });
  } catch (error) {
    yield put({ type: CATALOGUE_CONSTANTS.ERROR });
  }
}

/**
 * Watch fetch catalogue saga.
 */
export function* watchFetchCatalogue():void {
  yield* takeEvery(CATALOGUE_CONSTANTS.FETCH_CATALOGUE_REQUEST, fetchCatalogue);
}

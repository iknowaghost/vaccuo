import { fork } from 'redux-saga/effects';
import { watchFetchCatalogue } from './watchFetchCatalogue';
import { watchSearchCatalogue } from './watchSearchCatalogue';

/**
 * Catalogue saga root.
 */
export function* catalogue():void {
  yield fork(watchFetchCatalogue);
  yield fork(watchSearchCatalogue);
}

import React from 'react';
import classnames from 'classnames';
import CollapseMenu from 'components/ui/collapse-menu';

import style from './style.css';

// Set sidebar menu type
type SidebarMenuType = {
  tree: Object,
  isOpen: boolean,
  onSidebarMenuCloseClick: Function,
  onMenuCategoryMouseClick: Function,
  onMenuSubCategoryMouseClick: Function
}

/**
 * <SidebarMenu/>
 *
 * @returns {JSXElement}
 *
 * @param  {Object} props component props
 */
const SidebarMenu = ({
  tree,
  isOpen,
  onSidebarMenuCloseClick,
  onMenuCategoryMouseClick,
  onMenuSubCategoryMouseClick
}:SidebarMenuType):Object => {

  return (
    <div className={classnames(style.sidebarMenu, {[style.opened]: isOpen})}>
      <div className={style.close} onClick={onSidebarMenuCloseClick}>
        <div className={style.closeIcon}></div>
      </div>
      <ul className={style.itemList}>
        <CollapseMenu
          tree={tree}
          onNodeMouseClick={onMenuCategoryMouseClick}
          onLeafMouseClick={onMenuSubCategoryMouseClick}
          disableDefaultHeaderContent
        />
      </ul>
    </div>
  );
};

export default SidebarMenu;

import React from 'react';
import { Logo } from 'components';
import { SideBarContainer, MainMenuContainer } from 'containers';

type MainType = {
  children: Object
}

/**
 * <Main/> component
 *
 * @param {Object} props The props
 * @param {Object} props.children The section to be render
 * @return {JSXElement}
 */
const Main = ({ children }:MainType):Object => {

  return (
    <div className="app">
      <Logo />
      <MainMenuContainer />
      <SideBarContainer />
      <section className="content"> { children } </section>
    </div>
  );
}

export default Main;

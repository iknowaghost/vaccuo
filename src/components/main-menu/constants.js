export const TITLES = [
  {
    icon: 'iconHome',
    label: 'home'
  },
  {
    icon: 'iconSearch',
    label: 'search'
  },
  {
    icon: 'iconMenu',
    label: 'menu'
  }
];

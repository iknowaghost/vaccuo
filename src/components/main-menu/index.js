import React from 'react';
import classnames from 'classnames';

import { TITLES } from './constants';
import style from './style.css';

// Set main menu type
type MainMenuType = {
  className:string,
  onMainMenuItemClick:Function
}

/**
 * <MainMenu/>
 *
 * @returns {JSXElement}
 *
 * @param  {Object} props component props
 */
const MainMenu = ({
  className,
  onMainMenuItemClick
}:MainMenuType):Object => {

  // Generate menu items
  const items = TITLES.map((menu:Object, index:number):Object => {
    return (
      <li key={index} className={style.item} onClick={onMainMenuItemClick(menu.label)}>
        <div className={style[menu.icon]}></div>
      </li>
    );
  });

  return (
    <div className={classnames(style.mainMenu, style[className])}>
      <ul className={style.menu}>
        {items}
      </ul>
    </div>
  );
};

export default MainMenu;

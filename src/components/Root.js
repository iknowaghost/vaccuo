import React from 'react';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { ModalContainer } from 'containers';

import routes from './../routes';

/**
 * The root type defenition
 * @type {Object}
 */
type RootType = {
  store:Object
}

/**
 * <Root /> component
 *
 * @param {Object} props The props
 * @param {Object} props.store The store
 * @return {Object} An JSX element
 */
const Root = ({ store }:RootType):Object => (
  <Provider store={store}>
    <div>
      <Router history={browserHistory}>{routes}</Router>
      <ModalContainer />
    </div>
  </Provider>
)

export default Root;

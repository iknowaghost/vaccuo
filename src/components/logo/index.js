import React from 'react';

import style from './style.css';

const Logo = ():Object => {
  return (
    <div className={style.logo}></div>
  );
};

export default Logo;

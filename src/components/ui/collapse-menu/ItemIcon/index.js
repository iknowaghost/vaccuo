import React, { Component } from 'react';

import style from './style.css';

class Icon extends Component {

  /**
   * Render <Main/>
   *
   * @return {JSXElement}
   */
  render():Object {

    return (
      <div className={style.icon}>
        <img src={require('../../../../assets/icons/sidebar-menu/' + this.props.icon)}></img>
      </div>
    );
  }
}

export default Icon;

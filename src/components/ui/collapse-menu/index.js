import React from 'react';
import InfinityMenu from 'react-infinity-menu';

const CollapseMenu = (props:Object):Object => {
  return (
    <InfinityMenu {...props} >
      { props.children }
    </InfinityMenu>
  );
}

export default CollapseMenu;

import React, { Component } from 'react';
import Icon from '../ItemIcon';

import style from './style.css';

class Item extends Component {

  /**
   * On click node.
   *
   * @param {Object} event The a
   */
  onClick (event:Object) {
    this.props.onClick(event, this.props.data)
  }

  /**
   * Render <Main/>
   *
   * @return {JSXElement}
   */
  render():Object {

    return (
      <div className={style[this.props.data.customClass]} onClick={::this.onClick}>
        { this.props.data.icon ?
        <div className={style.iconWrapper}>
          <Icon icon={this.props.data.icon} />
        </div>
        : null }
        <label className={style.label}>{this.props.data.name}</label>
      </div>
    );
  }
}

export default Item;

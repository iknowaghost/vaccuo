import React from 'react';

import style from './style.css';

const SearchInput = ():Object => {
  return (
    <div className={style.content}>
      <div className={style.inputContainer}>
        <input className={style.input} type="text"></input>
        <div className={style.icon}></div>
      </div>

      <div className={style.label}>Type keyword to search</div>
    </div>
  );
}

export default SearchInput;

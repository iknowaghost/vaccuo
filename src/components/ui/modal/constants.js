export const DEFAULT_STYLE = {
  overlay : {
    backgroundColor: 'transparent',
    right          : '0',
    zIndex         : '1',
    transition     : 'right .25s ease-out'
  },
  content : {
    top         : '0',
    left        : '0',
    right       : '0',
    bottom      : '0',
    padding     : '0',
    background  : 'transparent',
    border      : 'none',
    backgroundColor : 'rgba(236, 236, 236, 0.9)'
  }
};

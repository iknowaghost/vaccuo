import React from 'react';
import Modal from 'react-modal';

import style from './style.css';
import { DEFAULT_STYLE } from './constants';

type ModalComponentType = {
  contentStyle:?Object,
  overlayStyle:?Object,
  children:?Object,
  onRequestClose:?Function
}

/**
 * <Modal/>
 * An abstraction of the react-modal.
 *
 * @see https://github.com/reactjs/react-modal
 * @returns {JSXElement}
 */
const ModalComponent = ({
  contentStyle,
  overlayStyle,
  children,
  onRequestClose,
  ...props
}:ModalComponentType):Object => {

  // Merge the default options with
  // the props
  const modalStyle = Object.assign({}, DEFAULT_STYLE, {
    content: Object.assign({}, DEFAULT_STYLE.content, contentStyle ? contentStyle : {}),
    overlay: Object.assign({}, DEFAULT_STYLE.overlay, overlayStyle ? overlayStyle : {})
  });

  /**
   * Render close icon.
   *
   * @method renderCloseIcon
   * @private
   *
   * @return {JSXElement}
   */
  const renderCloseIcon = ():Object => {
    return (
      <div
        className={style.close}
        onClick={onRequestClose}
      >
        <div className={style.icon}></div>
      </div>
    )
  }

  return (
    <Modal
      style={modalStyle}
      {...props}
    >
        { onRequestClose ? renderCloseIcon() : null }
        { children }
    </Modal>
  );
};

export default ModalComponent;

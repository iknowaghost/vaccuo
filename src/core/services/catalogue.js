import { products } from 'core/data';

/**
 * Catalogue Service
 * @type {Object}
 */
const catalogueService = {

  /**
   * Get all the products.
   *
   * @return {Promise<Array>} A resolved promise with an array products
   */
  getAll():Promise {
    return Promise.resolve(products);
  },

  /**
   * Search the products by query.
   *
   * @param {string} query The search parameter
   * @return {Promise<Array>} A resolved promise with an array products
   */
  search(query:?string):Promise {

    if(!query) {
      return Promise.resolve(products);
    }

    const filteredSearch = products.filter((item:Object):boolean => {
      return 0 === item.name.indexOf(query);
    });

    return Promise.resolve(filteredSearch);
  }
}

export default catalogueService;

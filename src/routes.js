import routes from 'views';
import Main from 'components/Main';

const configuration = {
  path: '/',
  childRoutes: routes,
  getComponents(location:?Object, cb:Function) {
    cb(null, Main);
  }
};

export default configuration;

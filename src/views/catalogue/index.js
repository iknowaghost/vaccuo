import Catalogue from './containers/CatalogueContainer';

module.exports = {
  path: 'catalogue',
  getComponents(location:?Object, cb:Function) {
    cb(null, Catalogue);
  },

  getChildRoutes(location:?Object, cb:Function) {
    cb(null, [{
      path: '(:type)',
      getComponents(location:?Object, cb:Function) {
        cb(null, Catalogue);
      }
    }]);
  }
};

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

/**
 * [CatalogueContainerType description]
 * @type {Object}
 */
type CatalogueContainerType = {
  params: Object,
  sidebar: Object
}

/**
 * <CatalogueContainer />
 *
 * @param {Object} props The props
 * @return {JSXElement}
 */
const CatalogueContainer = ({params, sidebar}:CatalogueContainerType):Object => {

  return (
    <div>
      <div> { params.type } </div>
      <div> { sidebar.selected.name } </div>
    </div>
  )
}

/**
 * Will subscribe to Redux store updates
 *
 * @method
 *
 * @param  {Object} state Store’s state
 * @param  {Object} ownProps Props
 * @return {Object} and returns an object to be passed as props
 */
const mapStateToProps = (state:Object, ownProps:Object):Object => {
  return {
    sidebar: state.sideBar,
    params: ownProps.params
  };
}

export default withRouter(connect(
  mapStateToProps
)(CatalogueContainer));

import React, { Component } from 'react';

/**
 * <Home />
 */
class Home extends Component {


  /**
   * Render <Home/>
   *
   * @return {JSXElement}
   */
  render():Object {

    return (
      <div>
        <section>
          <h2></h2>
        </section>
        { this.props.children }
      </div>
    );
  }
}

export default Home;

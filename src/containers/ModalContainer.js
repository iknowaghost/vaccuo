import React from 'react';
import { connect } from 'react-redux';
import { MODAL_NAMES } from 'store/constants/modal';
import SearchModal from './modals/SearchModal';

/**
 * Modal Container Type
 * @type {Object}
 */
type ModalContainerType = {
  name: ?string,
  data: ?Object
}

/**
 * <ModalContainer />
 *
 * @param {Object} props The props
 * @param {Object} props.name The modal name
 * @param {Object} props.data The modal data
 * @return {Object} the modal
 */
const ModalContainer = ({ name, data }:ModalContainerType):Object => {
  switch (name) {
  case MODAL_NAMES.SEARCH_MODAL:
    return <SearchModal {...data} />
  default:
    return <div />;
  }
}

/**
 * Will subscribe to Redux store updates
 *
 * @method
 *
 * @param  {Object} state Store’s state
 * @return {Object} and returns an object to be passed as props
 */
const mapStateToProps = (state:Object):ModalContainerType => {
  return state.modal;
}

export default connect(
  mapStateToProps
)(ModalContainer);

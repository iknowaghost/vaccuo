import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MainMenu } from 'components';
import { MODAL_NAMES } from 'store/constants/modal';
import { sideBarActionCreators, modalActionCreators }  from 'store/actions/';

/**
 * Main Menu Container Type
 * @type {Object}
 */
type MainMenuContainerType = {
  sidebar: ?Object,
  modalActions: ?Object,
  sideBarActions: ?Object
}

/**
 * <MainMenuContainer />
 *
 * @params {Object} props The props
 * @params {Boolean} props.isOpen Is the The sidebar open
 * @params {Object} props.modalActions The modal actions
 * @params {Object} props.sideBarActions The sideBar actions
 *
 * @return {Object} the sidebar container
 */
const MainMenuContainer = ({
  sidebar,
  modalActions,
  sideBarActions
}:MainMenuContainerType):Object => {

  /**
   * On main menu item click
   *
   * @method onMainMenuItemClick
   *
   * @param  {String} item The clicked item name.
   * @return {Function}
   */
  const onMainMenuItemClick = (item:string):Function => {
    return () => {
      switch(item) {
      case 'home':
        break;
      case 'search':
        modalActions.show(MODAL_NAMES.SEARCH_MODAL)
        break;
      case 'menu':
        sideBarActions.open()
        break;
      }
    }
  }

  return (
    <MainMenu
      className={sidebar.isOpen ? 'collapsed' : ''}
      onMainMenuItemClick={onMainMenuItemClick}
    />
  )
}

/**
 * Will subscribe to Redux store updates
 *
 * @method
 *
 * @param  {Object} state Store’s state
 * @return {Object} and returns an object to be passed as props
 */
const mapStateToProps = (state:Object):Object => {
  return {
    sidebar: state.sideBar
  }
}

/**
 * Bound to the store.
 * Will be merged into the component’s props.
 *
 * @method
 *
 * @param {Function} dispatch Dispatch function
 * @return {Object}
 */
const mapDispatchToProps = (dispatch:Function):Object => ({
  sideBarActions : bindActionCreators(sideBarActionCreators, dispatch),
  modalActions : bindActionCreators(modalActionCreators, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainMenuContainer);

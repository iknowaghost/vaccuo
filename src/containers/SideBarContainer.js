import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { SidebarMenu } from 'components';
import { sideBarActionCreators }  from 'store/actions/';

/**
 * Sidebar Container Type
 * @type {Object}
 */
export type SidebarContainerType = {
  sideBar: ?Object,
  sideBarActions: ?Object
}

/**
 * <SidebarMenuContainer />
 *
 * @return {Object} the sideBar container
 */
const SidebarMenuContainer = ({
  sideBar,
  sideBarActions
}:SidebarContainerType):Object => {

  /**
   * Handles the menu category mouse click
   *
   * @method onMenuCategoryMouseClick
   *
   * @param  {Object} event The clicked item name.
   * @param  {Object} tree The clicked item name.
   * @param  {Object} node The clicked item name.
   * @param  {Number} level The clicked item name.
   * @param  {String} keyPath The clicked item name.
   */
  const onMenuCategoryMouseClick = (event:Object, tree:Array<Object>) => {
    sideBarActions.updateMenu(tree);
  }

  /**
   * Handles the menu sub category mouse click
   *
   * @method onMenuSubCategoryMouseClick
   *
   * @param  {Object} event The event object
   * @param  {Object} data The data object
   */
  const onMenuSubCategoryMouseClick = (event:Object, data:Object) => {
    sideBarActions.selectMenuData(data);
    browserHistory.push(data.route);
  }

  return (
    <SidebarMenu
      isOpen={sideBar.isOpen}
      tree={sideBar.tree}
      onSidebarMenuCloseClick={sideBarActions.close}
      onMenuCategoryMouseClick={onMenuCategoryMouseClick}
      onMenuSubCategoryMouseClick={onMenuSubCategoryMouseClick}
    />
  )
}

/**
 * Will subscribe to Redux store updates
 *
 * @method
 *
 * @param  {Object} state Store’s state
 * @return {Object} and returns an object to be passed as props
 */
const mapStateToProps = (state:Object):Object => {
  return {
    sideBar: state.sideBar
  };
}

/**
 * Bound to the store.
 * Will be merged into the component’s props.
 *
 * @method
 *
 * @param {Function} dispatch Dispatch function
 * @return {Object}
 */
const mapDispatchToProps = (dispatch:Function):Object => ({
  sideBarActions : bindActionCreators(sideBarActionCreators, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SidebarMenuContainer);

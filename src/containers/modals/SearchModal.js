import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalActionCreators }  from 'store/actions/';
import { Modal, SearchInput } from 'components/ui';

/**
 * Search Modal Type.
 * @type {Object}
 */
type SearchModalType = {
  modalActions: Object
}

/**
 * <SearchModal />
 *
 * @param {Object} props The props
 * @param {Object} props.modalActions The actions of the modal action
 *
 * @return {JSXElement}
 */
const SearchModal = ({ modalActions }:SearchModalType):Object => {

  return (
    <Modal
      isOpen={true}
      onRequestClose={modalActions.hide}
    >
      <SearchInput />
    </Modal>
  )
}

/**
 * Bound to the store.
 * Will be merged into the component’s props.
 *
 * @method
 *
 * @param {Function} dispatch Dispatch function
 * @return {Object}
 */
const mapDispatchToProps = (dispatch:Function):Object => ({
  modalActions : bindActionCreators(modalActionCreators, dispatch)
});

export default connect(
  undefined,
  mapDispatchToProps
)(SearchModal);

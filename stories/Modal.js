import React, { Component } from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { Modal } from './../src/components/ui';

class SimpleModal extends Component {

  state = {
    isOpen: false
  }

  render() {
    return (
      <div>
        <button onClick={() => {
          this.setState({
            isOpen: true
          })
        }}
        > openModal </button>
        <Modal
          isOpen={this.state.isOpen}
          onRequestClose={() => {
            this.setState({
              isOpen: false
            })
          }}
        >
          <p>Modal Sample</p>
        </Modal>
      </div>
    )
  }
}

storiesOf('Modal', module)
  .add('simple', ():Object => (
    <SimpleModal />
  ));
